import es from "../i18n/es";
import en from "../i18n/en";
import cat from "../i18n/cat";
import { CustomWindow } from "../types/window.type";
import { LanguageType } from "../types/languages.type";

declare const window: CustomWindow;

const languages: LanguageType = {
  es,
  en,
  cat,
};

const defaultLanguage = window.navigator.language === "en" ? "en" : "es";

window.i18nData = languages[defaultLanguage];

window.i18n = (key: string) => window.i18nData[key as keyof LanguageType];

window.changeLanguage = (lang = "en") => {
  if (Object.keys(languages).includes(lang)) {
    window.i18nData = languages[lang as keyof LanguageType];
  } else {
    window.i18nData = languages.en;
  }
};
