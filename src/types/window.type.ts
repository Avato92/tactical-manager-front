export interface CustomWindow extends Window {
  i18nData: { [key: string]: string };
  i18n: (key: string) => string;
  changeLanguage: (key: string | undefined) => void;
}
