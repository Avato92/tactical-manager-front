export type LanguageType = { [key: string]: { [word: string]: string } };
