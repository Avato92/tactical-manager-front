import React from "react";
import { CustomWindow } from "./types/window.type";
import "./services/localizationService";
import I18nTest from "./components/I18nTest";

declare const window: CustomWindow;
class App extends React.Component {
  changeLanguage = (e: React.MouseEvent<HTMLButtonElement>) => {
    window.changeLanguage(e.currentTarget.name);
    this.forceUpdate();
  };

  render() {
    return (
      <div className="App">
        <p>Hola mundo</p>
        <p>
          {window.i18n("learnReact")}
        </p>
        <button onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.changeLanguage(e)} type="button" name="en">
          <img src="https://bit.ly/2NR57Sj" alt="en" />
        </button>
        <button onClick={(e: React.MouseEvent<HTMLButtonElement>) => this.changeLanguage(e)} type="button" name="es">
          <img src="https://bit.ly/36C7DV5" alt="hu" />
        </button>
        <I18nTest />
      </div>
    );
  }
}

export default App;
