import "../services/localizationService";
import { CustomWindow } from "../types/window.type";

declare const window: CustomWindow;

function I18nTest() {
  return <h1>{window.i18n("testingI18n")}</h1>;
}

export default I18nTest;
