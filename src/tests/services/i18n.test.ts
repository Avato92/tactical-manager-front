import "../../services/localizationService";
import { CustomWindow } from "../../types/window.type";

declare const window: CustomWindow;
describe("i18n testing", () => {
  test("it works", () => {
    expect(window.i18n("learnReact")).toEqual("Aprende React");
  });

  test("if I change language, returns a different string", () => {
    window.changeLanguage("en");
    expect(window.i18n("learnReact")).toEqual("Learn React");
  });

  test("if we changed to a unknow language, returns default language (en)", () => {
    window.changeLanguage("hu");
    expect(window.i18n("learnReact")).toEqual("Learn React");
  });
});
